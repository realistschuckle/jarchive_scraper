J-Archive Scraper
-----------------

This repository contains code to conditionally download the game pages
from www.j-archive.com and, then, extract the data from those pages.

JUST THE DATA

You can download the CSV files in dist/ to get the data from the game
show from the last time this repository was pushed. The CSVs are

* categories.csv: The list of all categories ever used in the game
* games.csv: The list of games and their air dates
* clues.csv: The list of all clues with relational identifiers for
             the data in categories.csv and games.csv

DEPENDENCIES

This works for macOS. It wouldn't be hard to add conditionals to
find the correct versions of sed and ls. The conditionals already
exist; you can just make some variables. If you want to do this
and make a merge request, I'm all for it!

* httpie (https://httpie.io/)
  A happy command line tool to easily make HTTP requests

* GNU sed (https://www.gnu.org/software/sed/)
  The lovely old school stream editor 

* GNU coreutils (https://www.gnu.org/software/coreutils)
  The best utils, all the utils

* GNU findutils (https://www.gnu.org/software/findutils)
  The best utils to find stuff

* Python 3 (https://www.python.org)
  Why not!

USE

1. Clone this repository
2. If there's a new season, remove the file
   data/season-page-urls.txt
3. If there are new episodes in a season, remove the file
   data/season-«season_number»-games.txt
4. Run download.sh. This can take a long time if there are no
   valid files in the data directory. I mean, a long time.
   Like hours. Even when it doesn't download anything, it takes
   a couple of minutes to complete. It's a shell script. Just
   run it and ignore it until it completes.
5. Run parse.py. This will convert the HTML files in the data
   directory into CSV files in the dist directory.
6. Use the CSVs!

