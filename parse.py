#!/usr/bin/env python3

import html
import os
import os.path
import re

DATA_PATH = "./data"


class Clue:
    def parse_clue(self, file):
        table_depth = 0
        for line in file:
            if "<table" in line:
                table_depth += 1
            elif "</table>" in line:
                table_depth -= 1
            if table_depth == 0 and "</td>" in line:
                break

            if "clue_value" in line or "clue_value_daily_double" in line:
                line = line.replace("clue_value_daily_double", "")
                line = line.replace("clue_value", "")
                line = line.replace("<td class=\"\">", "")
                line = line.replace("</td>", "")
                line = line.replace("$", "")
                line = line.replace("DD:", "")
                line = line.replace(",", "")
                line = line.strip()
                self.value = int(line)

            if "clue_text" in line:
                line = line[line.find(">") + 1:]
                line = line[:line.rfind("<")]
                line = html.unescape(line)
                line = line.strip()
                line = re.sub(r"<a href=\"([^\"]+)\"[^>]+>([^<]+)</a>", "[\\2](\\1)", line)
                self.question = line

            if "onmouseover" in line:
                line = line[line.find("correct_response"):]
                line = line[line.find("&gt;") + 4:]
                while line.startswith("&lt;"):
                    line = line[line.find("&gt;") + 4:]
                line = line[:line.find("&lt;")]
                line = html.unescape(line)
                line = line.strip()
                self.answer = line

    @property
    def is_empty(self):
        return not hasattr(self, "answer") or not hasattr(self, "question") or not hasattr(self, "value")

    def __repr__(self):
        if not hasattr(self, "value"): 
            return f"Clue<empty>"
        if not self.answer:
            raise Exception(f"{str(self.value)} missing answer")
        if not self.question:
            raise Exception(f"{str(self.value)} missing question")
        return f"Clue<{self.value}> [{self.answer}] {self.question}"


class Board:
    def __init__(self):
        self.categories = []
        self.clues = [[], [], [], [], []]

    def parse_board(self, file):
        table_depth = 1
        row_index = 0
        for i, line in enumerate(file):
            if "<table>" in line:
                table_depth += 1
            elif "</table>" in line:
                table_depth -= 1
            if table_depth == 0:
                break

            if "category_name" in line:
                line = re.sub(r"</?[^>]+>", "", line)
                line = line.strip()
                line = html.unescape(line)
                self.categories.append(line)

            if "<td class=\"clue\">" in line:
                clue = Clue()
                clue.parse_clue(file)
                if len(self.clues[row_index]) >= 6:
                    row_index += 1
                try:
                    clue.category = self.categories[len(self.clues[row_index])]
                    self.clues[row_index].append(clue)
                except Exception as e:
                    print(self.clues)
                    print(f"Error on line {i}")
                    raise e


class Game:
    def __init__(self):
        self.boards = []

    def parse_boards(self, file):
        find_round_table = False
        for line in file:
            if "<title>" in line:
                line = line.replace(" - ", "")
                line = re.sub(r"[<a-zA-Z#>!/,]+", "", line)
                line = line.strip()
                line = re.sub(r"\s+", " ", line)
                show_num, show_date = line.split(" ")
                self.number = show_num
                self.date = show_date
            if "jeopardy_round" in line:
                find_round_table = True
                continue
            if find_round_table and "<table class=\"round\">" in line:
                board = Board()
                board.parse_board(file)
                find_round_table = False
                self.boards.append(board)
        if not hasattr(self, "date"):
            raise Exception(f"Missing date for {file.name}")

    def __repr__(self):
        return f""" Game {self.number} ({self.date})"""


def csv_safe(s):
    wrap = False
    if "\"" in s:
        s = s.replace("\"", "\"\"")
        wrap = True
    if "," in s:
        wrap = True
    if wrap:
        return f"\"{s}\""
    return s


def binary_search(lst, item):
    first = 0
    last = len(lst)-1
    found = False

    while first<=last and not found:
        pos = 0
        midpoint = (first + last)//2
        if lst[midpoint] == item:
            pos = midpoint
            found = True
        else:
            if item < lst[midpoint]:
                last = midpoint-1
            else:
                first = midpoint+1
    return pos, found


def parse_data():
    games = []
    for entry in os.listdir(path=DATA_PATH):
        if not entry.endswith(".html"):
            continue
        with open(os.path.join(DATA_PATH, entry)) as game_file:
            game = Game()
            try:
                game.parse_boards(game_file)
            except Exception as e:
                print(e)
                raise e
            games.append(game)
    categories = sorted(set([c for g in games for b in g.boards for c in b.categories]))
    games = sorted(games, key=lambda x: x.date)
    
    with open("dist/categories.csv", "w") as categories_file:
        print("id,name", file=categories_file)
        for id, name in enumerate(categories):
            print(f"{id+1},{csv_safe(name)}", file=categories_file)

    with open("dist/games.csv", "w") as games_file:
        print("id,episode_id,aired", file=games_file)
        for id, game in enumerate(games):
            print(f"{id+1},{csv_safe(game.number)},{csv_safe(game.date)}", file=games_file)

    with open("dist/clues.csv", "w") as clues_file:
        clue_id = 1
        print("id,row,column,answer,question,game_id,board_index,value,category_id", file=clues_file)
        for game_id, game in enumerate(games):
            for board_index, board in enumerate(game.boards):
                for row_index, row in enumerate(board.clues):
                    for column_index, clue in enumerate(row):
                        if clue.is_empty:
                            continue
                        category_id, found = binary_search(categories, clue.category)
                        if not found:
                            raise Exception(f"Encountered category '{clue.category}' that doesn't seem to exist.")
                        print(f"{clue_id},{row_index+1},{column_index+1},{csv_safe(clue.answer)},{csv_safe(clue.question)},{game_id+1},{board_index},{clue.value},{category_id+1}", file=clues_file)
                        clue_id += 1


if __name__ == "__main__":
    parse_data()

