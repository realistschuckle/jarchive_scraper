if [[ ! -x `which https` ]]
then
  echo "Please install httpie"
  exit 1
fi

if [[ ! -x `which gsed` ]]
then
  echo "Please install GNU Utils with g-prefix"
  exit 2
fi

mkdir -p data

pushd data 1>/dev/null 2>/dev/null

if [[ ! -f 'season-page-urls.txt' ]]
then
  https j-archive.com/listseasons.php | \
    ggrep --color -oe 'showseason.php?season=[[:digit:]]\+' | \
    gsed 's/^/https:\/\/j-archive.com\//' > season-page-urls.txt
fi

for url in `gcat season-page-urls.txt`
do
  file_name="$(echo $url | gsed 's/https:\/\/j-archive.com\/showseason.php?//g' | gsed 's/=/-/g' | gxargs -I {} echo "{}-games.txt")"

  if [[ ! -f "${file_name}" ]]
  then
    https "$url" | \
      ggrep -oe 'showgame.php?game_id=[[:digit:]]\+' > "${file_name}"
  else
    echo "Skipping:      ${file_name}"
  fi
done

for game in `gls *.html`
do
  last_line=$(gtail -n 1 "$game" | gtr -d '\n')
  if [ "${last_line}" != "</html>" ]
  then
    echo "Removing:      ${game}"
    rm $game
  else
    echo "Seems ok:      ${game}"
  fi
done

for season in `gls *-games.txt`
do
  season_name="$(echo ${season%.*} | gsed 's/-games//')"
  for path in `gcat ${season}`
  do
    file_name="$(echo $path | gsed 's/showgame.php?game_id=//' | gxargs -I {} echo "${season_name}-game-{}.html")"

    if [[ ! -f "${file_name}" ]]
    then
      echo "Downloading: ${file_name}"
      url="https://www.j-archive.com/${path}"
      https -p b "$url" > $file_name
      sleep 0.5 # Don't overwhelm j-archive.com
    else
      echo "Skipping:    ${file_name}"
    fi
  done
done

popd 1>/dev/null 2>/dev/null

